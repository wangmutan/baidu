# baidu

#### 项目介绍
[百度一下，你就上当](https://wangmutan.gitee.io/baidu)

#### 软件架构
软件架构说明：开发人员都不用百度，你懂的！


#### 安装教程

无需安装，直接使用 :point_right: [百度一下，你就上当](https://wangmutan.gitee.io/baidu)

#### 使用说明

屠龙宝刀，即点即用 :point_right: [百度一下，你就上当](https://wangmutan.gitee.io/baidu)

#### 参与贡献

1. Fork 本项目
2. 点击 index.html，直接访问！
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

> 上面这一段是码云自己生成的，与我无关，哈哈哈